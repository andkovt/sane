using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Sane.Logging;

namespace Sane.Events
{
    public class EventManager : IEventManager
    {
        private readonly ILogger logger;
        private readonly IDictionary<string, IList<Action<Event>>> registeredObservers;

        /// <summary>
        /// EventManager constructor
        /// </summary>
        /// <param name="logger"></param>
        public EventManager(ILogger logger)
        {
            this.logger = logger;
            this.registeredObservers = new Dictionary<string, IList<Action<Event>>>();
        }
        
        /// <inheritdoc />
        public void RegisterObserver(string eventName, Action<Event> observer)
        {
            if (!registeredObservers.ContainsKey(eventName)) {
                registeredObservers[eventName] = new Collection<Action<Event>>();
            }
            
            logger.Debug($"Registering an observer for event '{eventName}'");
            registeredObservers[eventName].Add(observer);
        }

        /// <inheritdoc />
        public void DeregisterObserver(string eventName, Action<Event> observer)
        {
            if (!registeredObservers.ContainsKey(eventName)) {
                return;
            }
            
            logger.Debug($"Removing observer from event '{eventName}'");
            registeredObservers[eventName].Remove(observer);
        }

        /// <inheritdoc />
        public void ClearEventObservers(string eventName)
        {
            if (!registeredObservers.ContainsKey(eventName)) {
                return;
            }
            
            logger.Debug($"Clearing all '{eventName}' observers");
            registeredObservers[eventName].Clear();;
        }

        /// <inheritdoc />
        public void RaiseEvent(string eventName, object content)
        {
            if (!registeredObservers.ContainsKey(eventName)) {
                logger.Info($"Skipping event raise for '{eventName}' because it has no observers");
                return;
            }
            
            var eventData = new Event {EventName = eventName, Content = content, Handled = false};
            for (var i = 0; i < registeredObservers[eventName].Count; i++) {
                registeredObservers[eventName][i](eventData);

                if (eventData.Handled) {
                    break;
                }
            }
        }
    }
}