namespace Sane.Logging.Transformers
{
    /// <summary>
    /// Log message transformer
    /// </summary>
    public interface ITransformer
    {
        /// <summary>
        /// Transforms the message
        /// </summary>
        /// <param name="message">Message to transform</param>
        /// <returns>Transformed message</returns>
        Message Transform(Message message);
    }
}