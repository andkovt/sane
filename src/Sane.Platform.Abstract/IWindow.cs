using OpenTK;
using Sane.Common;

namespace Sane.Platform.Abstract
{
    /// <summary>
    /// Application window
    /// </summary>
    public interface IWindow : IUpdateable
    {
        /// <summary>
        /// Window title
        /// </summary>
        string Title { get; set; }
        
        /// <summary>
        /// Window size
        /// </summary>
        Vector2 Size { get; set; }
        
        /// <summary>
        /// VSync toggle
        /// </summary>
        bool VSync { get; set; }
        
        /// <summary>
        /// Opens the window
        /// </summary>
        public void Open();
        
        /// <summary>
        /// Closes the window
        /// </summary>
        public void Close();
    }
}