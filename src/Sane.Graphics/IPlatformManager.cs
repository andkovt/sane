using System.Collections.Generic;
using Sane.Platform.Abstract;

namespace Sane.Graphics
{
    /// <summary>
    /// Provides and manages available graphics platforms
    /// </summary>
    public interface IPlatformManager
    {
        /// <summary>
        /// Current platform
        /// </summary>
        IPlatform CurrentPlatform { get; }
        
        /// <summary>
        /// Checks and returns available rendering platforms
        /// </summary>
        /// <returns>Available platforms</returns>
        IList<IPlatform> GetSupporterPlatforms();
        
        /// <summary>
        /// Retrieves all available platforms
        /// </summary>
        /// <returns>All available platforms</returns>
        IList<IPlatform> GetAllPlatforms();
        
        /// <summary>
        /// Selects and initializes rendering platform
        /// </summary>
        /// <param name="platform">Platform to use</param>
        PlatformHandler SelectPlatform(IPlatform platform);
    }
}