namespace Sane.Events
{
    /// <summary>
    /// Event struct
    /// </summary>
    public class Event
    {
        /// <summary>
        /// Name of the event
        /// </summary>
        public string EventName;
        
        /// <summary>
        /// Toggle indicating if the event was handled
        /// </summary>
        public bool Handled;
        
        /// <summary>
        /// Event content
        /// </summary>
        public object Content;
    }
}