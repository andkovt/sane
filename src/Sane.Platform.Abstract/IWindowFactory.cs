using System.Numerics;
using Vector2 = OpenTK.Vector2;

namespace Sane.Platform.Abstract
{
    /// <summary>
    /// Window factory
    /// </summary>
    public interface IWindowFactory
    {
        /// <summary>
        /// Create a new window instance
        /// </summary>
        /// <param name="title">Window title</param>
        /// <param name="size">Window size</param>
        /// <returns></returns>
        IWindow Create(string title, Vector2 size);
    }
}