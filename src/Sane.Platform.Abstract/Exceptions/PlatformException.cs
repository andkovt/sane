using System;
using System.Runtime.Serialization;
using Sane.Common.Exceptions;

namespace Sane.Platform.Abstract.Exceptions
{
    public class PlatformException : SaneException
    {
        /// <inheritdoc />
        public PlatformException()
        {
        }

        /// <inheritdoc />
        protected PlatformException(SerializationInfo? info, StreamingContext context) : base(info, context)
        {
        }

        /// <inheritdoc />
        public PlatformException(string? message) : base(message)
        {
        }

        /// <inheritdoc />
        public PlatformException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}