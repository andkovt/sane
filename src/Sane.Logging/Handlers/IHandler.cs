namespace Sane.Logging.Handlers
{
    /// <summary>
    /// Log message handler
    /// </summary>
    public interface IHandler
    {
        /// <summary>
        /// Handles the log message
        /// </summary>
        /// <param name="message">Message to handle</param>
        void Handle(Message message);
    }
}