using System.Collections;
using Sane.Logging;

namespace Sane.Platform.Abstract
{
    /// <summary>
    /// Sane Platform
    /// </summary>
    public interface IPlatform
    {
        /// <summary>
        /// Platform name
        /// </summary>
        string Name { get; }
        
        /// <summary>
        /// Checks if platform is supported on the machine
        /// </summary>
        /// <returns></returns>
        bool Supported();
        
        /// <summary>
        /// Initializes platform
        /// </summary>
        PlatformAdapter Initialize(ILogger logger);
    }
}