using Sane.Logging.Conditions;
using Sane.Logging.Handlers;
using Sane.Logging.Transformers;

namespace Sane.Logging
{
    /// <summary>
    /// Configurable logger
    /// </summary>
    public class ConfigurableLogger : ILogger
    {
        private readonly IHandler handler;
        private readonly ITransformer transformer;
        private readonly ICondition condition;
        
        /// <summary>
        /// Configurable logger constructor
        /// </summary>
        /// <param name="handler">Handler which will be used to handle messages</param>
        /// <param name="transformer">Transformer which will be used to transform messages</param>
        /// <param name="condition">Condition against which to messages will be evaluated</param>
        public ConfigurableLogger(IHandler handler, ITransformer transformer, ICondition condition)
        {
            this.handler = handler;
            this.transformer = transformer;
            this.condition = condition;
        }

        public void Debug(string message)
        {
            Log(new Message {Level = MessageLevel.Debug, Content = message});
        }

        public void Info(string message)
        {
            Log(new Message {Level = MessageLevel.Info, Content = message});
        }

        public void Warning(string message)
        {
            Log(new Message {Level = MessageLevel.Warning, Content = message});
        }

        public void Error(string message)
        {
            Log(new Message {Level = MessageLevel.Error, Content = message});
        }

        public void Critical(string message)
        {
            Log(new Message {Level = MessageLevel.Critical, Content = message});
        }

        /// <inheritdoc/>
        public void Log(Message message)
        {
            if (condition != null && !condition.Evaluate(message)) {
                return; // Discard the message because it does not match the condition
            }

            handler.Handle(transformer?.Transform(message) ?? message);
        }
    }
}