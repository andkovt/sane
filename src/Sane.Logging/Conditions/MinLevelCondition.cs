namespace Sane.Logging.Conditions
{
    /// <summary>
    /// Condition checks if message level is higher than defined
    /// </summary>
    public class MinLevelCondition : ICondition
    {
        private readonly MessageLevel minLevel;
        
        /// <summary>
        /// MinLevelCondition constructor
        /// </summary>
        /// <param name="minLevel">Minimal level</param>
        public MinLevelCondition(MessageLevel minLevel)
        {
            this.minLevel = minLevel;
        }
        
        /// <inheritdoc/>
        public bool Evaluate(Message message)
        {
            return message.Level > minLevel;
        }
    }
}