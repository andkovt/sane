using GLFW;
using OpenTK;
using Sane.Platform.Abstract;

namespace Sane.Platform.OpenGL
{
    public class WindowFactory : IWindowFactory
    {
        /// <inheritdoc />
        public IWindow Create(string title, Vector2 size)
        {
            var windowHandler = Glfw.CreateWindow((int) size.X, (int) size.Y, title, GLFW.Monitor.None, GLFW.Window.None);
            return new Window(windowHandler, title, size);
        }
    }
}