using Sane.Logging;
using Sane.Platform.Abstract;
using Sane.Platform.Abstract.Exceptions;

namespace Sane.Graphics
{
    /// <summary>
    /// Handles platform related actions
    /// </summary>
    public class PlatformHandler
    {
        private readonly IPlatform platform;
        private readonly ILogger logger;

        /// <summary>
        /// Window manager
        /// </summary>
        public WindowManager WindowManager { get; private set; }

        /// <summary>
        /// PlatformHandler constructor
        /// </summary>
        /// <param name="platform"></param>
        /// <param name="logger"></param>
        public PlatformHandler(IPlatform platform, ILogger logger)
        {
            this.platform = platform;
            this.logger = logger;
        }
        
        /// <summary>
        /// Initializes platform
        /// </summary>
        public void Initialize()
        {
            logger.Info($"Starting to initialize platform {platform.Name}");

            PlatformAdapter adapter;
            try {
                adapter = platform.Initialize(logger);
            } catch (PlatformException e) {
                logger.Critical(e.Message);
                logger.Critical(e.StackTrace);

                throw e;
            }

            WindowManager = new WindowManager(adapter.WindowFactory);
        }
    }
}