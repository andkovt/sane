using OpenTK;
using Sane.Platform.Abstract;

namespace Sane.Graphics
{
    public class WindowManager
    {
        private readonly IWindowFactory windowFactory;
        
        public IWindow CurrentWindow { get; set; }

        public WindowManager(IWindowFactory windowFactory)
        {
            this.windowFactory = windowFactory;
        }
        
        public IWindow Create(string title, Vector2 size)
        {
            return this.windowFactory.Create(title, size);
        }
    }
}