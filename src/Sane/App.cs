using System;
using OpenTK;
using Sane.Common.Exceptions;
using Sane.Events;
using Sane.Graphics;
using Sane.Logging;
using Sane.Logging.Handlers;

namespace Sane
{
    public class App : IDisposable
    {
        public IEventManager EventManager { get; protected set; }
        public ILogger Logger { get; protected set; }
        
        public IPlatformManager PlatformManager { get; protected set; }
        public PlatformHandler PlatformHandler { get; protected set; }
        
        public bool Running { get; private set; }
        public string Name { get; set; }

        private bool initialized = false;
        
        /// <summary>
        /// App constructor
        /// </summary>
        /// <param name="applicationName"></param>
        /// <param name="defaultWindowSize"></param>
        public App(string applicationName, Vector2 defaultWindowSize)
        {
            Name = applicationName;
            
            // Setup default logger
            Logger = new ConfigurableLogger(new ConsoleHandler(), null, null);
            Logger.Info("Default logger created");
            
            // Setup platform manager
            Logger.Info("Setting up platform manager");
            PlatformManager = new PlatformManager(Logger);
        }
        
        /// <summary>
        /// Initializes application
        /// </summary>
        public void Setup()
        {
            Configure();
            ConfigureDefaults();
        }
        
        /// <summary>
        /// Runs application
        /// </summary>
        /// <exception cref="SaneException"></exception>
        public void Run()
        {
            if (initialized) {
                throw new SaneException("Application needs to be initialized before calling run");
            }
            
            BeforeRun();
            InitializePlatform();
            Running = true;

            var window = PlatformHandler.WindowManager.Create("Test", new Vector2(1024, 720));
            PlatformHandler.WindowManager.CurrentWindow = window;
            
            window.Open();
            
            while (Running) {
                // @todo Do it
            }
        }
        
        /// <summary>
        /// Stops the application
        /// </summary>
        public void Stop()
        {
            Running = false;
            AfterStop();
        }
        
        /// <inheritdoc />
        public void Dispose()
        {
        }
        
        /// <summary>
        /// Configures application
        /// </summary>
        protected virtual void Configure()
        {
            
        }
        
        /// <summary>
        /// Executes before running application
        /// </summary>
        protected virtual void BeforeRun()
        {
            
        }
        
        /// <summary>
        /// Executes after stopping application
        /// </summary>
        protected virtual void AfterStop()
        {
            
        }
        
        /// <summary>
        /// Configures defaults
        /// </summary>
        private void ConfigureDefaults()
        {
            if (EventManager == null) {
                EventManager = new EventManager(Logger);
            }
            
            PlatformHandler = PlatformManager.SelectPlatform(PlatformManager.GetSupporterPlatforms()[0]);
        }
        
        /// <summary>
        /// Initializes platform
        /// </summary>
        private void InitializePlatform()
        {
            PlatformHandler.Initialize();
        }
    }
}