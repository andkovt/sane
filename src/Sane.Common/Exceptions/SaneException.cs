using System;
using System.Runtime.Serialization;

namespace Sane.Common.Exceptions
{
    public class SaneException : Exception
    {
        /// <inheritdoc />
        public SaneException()
        {
        }

        /// <inheritdoc />
        protected SaneException(SerializationInfo? info, StreamingContext context) : base(info, context)
        {
        }

        /// <inheritdoc />
        public SaneException(string? message) : base(message)
        {
        }

        /// <inheritdoc />
        public SaneException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}