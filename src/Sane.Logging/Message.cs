namespace Sane.Logging
{
    /// <summary>
    /// Log message
    /// </summary>
    public struct Message
    {
        /// <summary>
        /// Message type
        /// </summary>
        public MessageLevel Level;
        
        /// <summary>
        /// Message content
        /// </summary>
        public string Content;
    }
}