using System.Collections.Generic;

namespace Sane
{
    /// <summary>
    /// Active application registry
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public static class AppRegistry<T> where T : App
    {
        /// <summary>
        /// Current application instance
        /// </summary>
        public static T Current;
        
        /// <summary>
        /// All registered applications
        /// </summary>
        public static readonly IDictionary<string, T> Applications = new Dictionary<string, T>();

        /// <summary>
        /// Registers application
        /// </summary>
        /// <param name="id">Application id</param>
        /// <param name="application">Application instance</param>
        public static void Register(string id, T application)
        {
            Applications.Add(id, application);;
            if (Current == null) {
                Current = application;
            }
        }
        
        /// <summary>
        /// Switches current application
        /// </summary>
        /// <param name="newApplicationId"></param>
        public static void SwitchCurrent(string newApplicationId)
        {
            Current = Applications[newApplicationId];
        }
    }
}