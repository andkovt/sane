using OpenTK;

namespace Sandbox
{
    public class Program
    {
        public static void Main(string[] args)
        {
            using (var app = new SandboxApp("Sandbox App", Vector2.One)) {
                app.Setup();
                app.Run();
            }
        }
    }
}