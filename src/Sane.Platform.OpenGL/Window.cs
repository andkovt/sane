using System;
using GLFW;
using Sane.Platform.Abstract;
using OpenTK;

namespace Sane.Platform.OpenGL
{
    public class Window : IWindow
    {
        private readonly GLFW.Window windowHandler;

        /// <inheritdoc />
        public string Title
        {
            get => currentTitle;
            set => UpdateTitle(value);
        }

        /// <inheritdoc />
        public Vector2 Size
        {
            get => currentSize;
            set => UpdateSize(value);
        }

        /// <inheritdoc />
        public bool VSync
        {
            get => vSyncState;
            set => ToggleVsync(value);
        }

        private bool vSyncState;
        private string currentTitle;
        private Vector2 currentSize;

        /// <summary>
        /// Window constructor
        /// </summary>
        /// <param name="windowHandler"></param>
        /// <param name="title"></param>
        /// <param name="size"></param>
        public Window(GLFW.Window windowHandler, string title, Vector2 size)
        {
            this.windowHandler = windowHandler;
            currentTitle = title;
            currentSize = size;
        }

        /// <inheritdoc />
        public void Update()
        {
            Glfw.PollEvents();
            Glfw.SwapBuffers(windowHandler);
        }

        /// <inheritdoc />
        public void Open()
        {
            Glfw.MakeContextCurrent(windowHandler);
            Glfw.SetWindowUserPointer(windowHandler, IntPtr.Zero);

            Glfw.SetWindowSizeCallback(windowHandler, OnResize);
        }

        /// <inheritdoc />
        public void Close()
        {
            Glfw.DestroyWindow(windowHandler);
        }
        
        /// <summary>
        /// Sets VSync state
        /// </summary>
        /// <param name="state"></param>
        private void ToggleVsync(bool state)
        {
            vSyncState = state;
            
            if (state) {
                Glfw.SwapInterval(1);
                return;
            }
            
            Glfw.SwapInterval(0);
        }
        
        /// <summary>
        /// Updates window title
        /// </summary>
        /// <param name="newTitle"></param>
        private void UpdateTitle(string newTitle)
        {
            Glfw.SetWindowTitle(windowHandler, newTitle);
            currentTitle = newTitle;
        }
        
        /// <summary>
        /// Updates window size
        /// </summary>
        /// <param name="size"></param>
        private void UpdateSize(Vector2 size)
        {
            Glfw.SetWindowSize(windowHandler, (int) size.X, (int) size.Y);
            currentSize = size;
        }
        
        /// <summary>
        /// On Window Resize
        /// </summary>
        /// <param name="ptr"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        private void OnResize(IntPtr ptr, int width, int height)
        {
            currentSize.X = width;
            currentSize.Y = height;
            
            Console.WriteLine($"Resizeing {currentSize}");
        }
    }
}