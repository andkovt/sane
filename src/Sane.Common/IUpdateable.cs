namespace Sane.Common
{
    /// <summary>
    /// Represents class that can receive update events
    /// </summary>
    public interface IUpdateable
    {
        /// <summary>
        /// Performs update
        /// </summary>
        void Update();
    }
}