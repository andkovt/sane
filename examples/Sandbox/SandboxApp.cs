using System;
using OpenTK;
using Sane;

namespace Sandbox
{
    public class SandboxApp : App
    {
        /// <inheritdoc />
        public SandboxApp(string applicationName, Vector2 defaultWindowSize) : base(applicationName, defaultWindowSize)
        {
        }
    }
}