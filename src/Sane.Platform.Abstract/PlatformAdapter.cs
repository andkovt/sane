namespace Sane.Platform.Abstract
{
    public class PlatformAdapter
    {
        public IWindowFactory WindowFactory { get; set; }
    }
}