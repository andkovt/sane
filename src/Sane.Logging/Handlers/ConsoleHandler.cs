using System;

namespace Sane.Logging.Handlers
{
    /// <inheritdoc/>
    public class ConsoleHandler : IHandler
    {
        /// <inheritdoc/>
        public void Handle(Message message)
        {
            var prevColor = Console.ForegroundColor;

            switch (message.Level) {
                case MessageLevel.Debug:
                    Console.ForegroundColor = ConsoleColor.Gray;
                    break;
                case MessageLevel.Info:
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
                case MessageLevel.Warning:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                case MessageLevel.Error:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;
                case MessageLevel.Critical:
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    break;
            }
            
            Console.WriteLine(message.Content);
            Console.ForegroundColor = prevColor;
        }
    }
}