namespace Sane.Logging
{
    /// <summary>
    /// Responsible for logging messages
    /// </summary>
    public interface ILogger
    {
        /// <summary>
        /// Log debug message
        /// </summary>
        /// <param name="message"></param>
        void Debug(string message);
        
        /// <summary>
        /// Log info message
        /// </summary>
        /// <param name="message"></param>
        void Info(string message);
        
        /// <summary>
        /// Log warning message
        /// </summary>
        /// <param name="message"></param>
        void Warning(string message);
        
        /// <summary>
        /// Log error message
        /// </summary>
        /// <param name="message"></param>
        void Error(string message);
        
        /// <summary>
        /// Log critical message
        /// </summary>
        /// <param name="message"></param>
        void Critical(string message);
        
        /// <summary>
        /// Logs the message
        /// </summary>
        /// <param name="message">Message to log</param>
        void Log(Message message);
    }
}