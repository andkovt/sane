using System.Collections.Generic;
using System.Linq;
using Sane.Logging;
using Sane.Platform.Abstract;
using Sane.Platform.OpenGL;

namespace Sane.Graphics
{
    public class PlatformManager : IPlatformManager
    {
        /// <summary>
        /// Logger
        /// </summary>
        private readonly ILogger logger;

        /// <inheritdoc />
        public IPlatform CurrentPlatform { get; private set; }
        
        /// <summary>
        /// Available platforms
        /// </summary>
        private readonly IList<IPlatform> availablePlatforms;
    
        /// <summary>
        /// PlatformManager constructor
        /// </summary>
        public PlatformManager(ILogger logger)
        {
            this.logger = logger;
            
            availablePlatforms = new List<IPlatform>();
            LoadAvailablePlatforms();
        }

        /// <inheritdoc />
        public IList<IPlatform> GetSupporterPlatforms()
        {
            return availablePlatforms.Where(p => p.Supported()).ToList();
        }

        /// <inheritdoc />
        public IList<IPlatform> GetAllPlatforms()
        {
            return availablePlatforms;
        }

        /// <inheritdoc />
        public PlatformHandler SelectPlatform(IPlatform platform)
        {
            logger.Info($"Selecting platform {platform.Name}");
            CurrentPlatform = platform;
            
            return new PlatformHandler(CurrentPlatform, logger);
        }
        
        /// <summary>
        /// Loads available platforms
        /// </summary>
        private void LoadAvailablePlatforms()
        {
            logger.Info("Loading available platforms");
            
            availablePlatforms.Add(new OpenGLPlatform());

            foreach (var platform in availablePlatforms) {
                logger.Info($"Available platform: {platform.Name}");
            }
        }
    }
}