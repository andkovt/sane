using GLFW;
using Sane.Logging;
using Sane.Platform.Abstract;
using Sane.Platform.Abstract.Exceptions;

namespace Sane.Platform.OpenGL
{
    /// <summary>
    /// OpenGL Platform
    /// </summary>
    public class OpenGLPlatform : IPlatform
    {
        /// <inheritdoc />
        public string Name { get; } = "OpenGL";

        /// <inheritdoc />
        public bool Supported()
        {
            return true;
        }

        /// <inheritdoc />
        public PlatformAdapter Initialize(ILogger logger)
        {
            logger.Info("Initializing GLFW");
            if (!Glfw.Init()) {
                throw new PlatformException("Failed to initialize GLFW");
            }

            return new PlatformAdapter
            {
                WindowFactory = new WindowFactory()
            };
        }
    }
}