namespace Sane.Logging
{
    /// <summary>
    /// Log message type
    /// </summary>
    public enum MessageLevel
    {
        Debug,
        Info,
        Warning,
        Error,
        Critical
    }
}