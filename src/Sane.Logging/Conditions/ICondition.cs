namespace Sane.Logging.Conditions
{
    /// <summary>
    /// Logging condition
    /// </summary>
    public interface ICondition
    {
        /// <summary>
        /// Checks if message passes the condition
        /// </summary>
        /// <param name="message">Message to test</param>
        /// <returns>True if message passes, false if it doesn't</returns>
        bool Evaluate(Message message);
    }
}