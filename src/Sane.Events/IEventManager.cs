using System;

namespace Sane.Events
{
    /// <summary>
    /// Responsible for managing and handling of events
    /// </summary>
    public interface IEventManager
    {
        /// <summary>
        /// Registers an event observer
        /// </summary>
        /// <param name="eventName">Event name</param>
        /// <param name="observer">Observer action</param>
        void RegisterObserver(string eventName, Action<Event> observer);

        /// <summary>
        /// Removes observer
        /// </summary>
        /// <param name="eventName">Event name</param>
        /// <param name="observer">Observer</param>
        void DeregisterObserver(string eventName, Action<Event> observer);
        
        /// <summary>
        /// Removes event observers
        /// </summary>
        /// <param name="eventName"></param>
        void ClearEventObservers(string eventName);
        
        /// <summary>
        /// Raises an event
        /// </summary>
        /// <param name="eventName">Event to raise</param>
        /// <param name="content">Content</param>
        void RaiseEvent(string eventName, object content);
    }
}